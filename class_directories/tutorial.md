Most beginner python tutorials will build the entire example inside the one main.py file.  This makes for  pretty messy file pretty quickly.  So it is a good idea to separate your code into separate distinct files, and classes are a good chunk of code to tuck away.

During this quick tutorial, we will demonstrate how to create a class inside a separate file, then import and use it from our main python file.
